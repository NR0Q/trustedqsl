#include <string>
#include <iostream>
#include <gtest/gtest.h>

#include "../src/tqsllib.h"

#if __cplusplus <= 199711L
#define override
#define nullptr NULL
#endif

/* This data structure was copied from src/tqslconvert.cpp */
static tqsl_adifFieldDefinitions adif_qso_record_fields[] = {
        { "CALL", "", TQSL_ADIF_RANGE_TYPE_NONE, TQSL_CALLSIGN_MAX, 0, 0, NULL },
        { "BAND", "", TQSL_ADIF_RANGE_TYPE_NONE, TQSL_BAND_MAX, 0, 0, NULL },
	{ "MODE", "", TQSL_ADIF_RANGE_TYPE_NONE, TQSL_MODE_MAX, 0, 0, NULL },
	{ "QSO_DATE", "", TQSL_ADIF_RANGE_TYPE_NONE, 10, 0, 0, NULL },
	{ "TIME_ON", "", TQSL_ADIF_RANGE_TYPE_NONE, 10, 0, 0, NULL },
	{ "FREQ", "", TQSL_ADIF_RANGE_TYPE_NONE, TQSL_FREQ_MAX, 0, 0, NULL },
	{ "STATION_CALLSIGN", "", TQSL_ADIF_RANGE_TYPE_NONE, TQSL_CALLSIGN_MAX, 0, 0, NULL },
	{ "eor", "", TQSL_ADIF_RANGE_TYPE_NONE, 0, 0, 0, NULL },
};

/* This data structure was copied from src/tqslconvert.cpp */
static const char *notypes[] = { "D", "T", "M", "C", "N", "S", "B", "E", "L", "" };     // "C" is ADIF 1.0 for "S"; also "I" and "G" in ADIX

/* tqsl_getADIFField() requires a custom allocator to be passed */
unsigned char *my_malloc(size_t size) {
	return (unsigned char *)malloc(size);
}


class FileDecode : public ::testing::Test {
protected:
	std::string fixture;

	void SetUp() override {
		::testing::Test::SetUp();
		int retval;
		const char *path_env = getenv("DATADIR");
		ASSERT_NE(path_env, (void *)NULL);
		ASSERT_STRNE(path_env, "");
		fixture = path_env;
		fixture += "/simple.adif";

		retval = tqsl_init();
		ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	}
};

TEST_F(FileDecode, OpenTest) {
	tQSL_ADIF adif = nullptr;
	int retval;

	retval = tqsl_beginADIF(&adif, fixture.c_str());
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	ASSERT_NE(adif, (void *)NULL);

	retval = tqsl_endADIF(&adif);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	ASSERT_EQ(adif, (void *)NULL);
}

TEST_F(FileDecode, FieldTest) {
	tQSL_ADIF adif = nullptr;
	int retval;

	retval = tqsl_beginADIF(&adif, fixture.c_str());
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();

	tqsl_adifFieldResults field;
	TQSL_ADIF_GET_FIELD_ERROR status;
	retval = tqsl_getADIFField(adif, &field, &status, adif_qso_record_fields, notypes, my_malloc);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_EQ(status, TQSL_ADIF_GET_FIELD_SUCCESS) << "Status: " << status << ", Value: " << tqsl_adifGetError(status);
	//std::cout << "Line: " << field.line_no << ", Name: " << field.name << ", Type: " << field.type << ", size: " << field.size << ", Data: " << field.data << std::endl;

	retval = tqsl_endADIF(&adif);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
}
