#include <string>
#include <iostream>
#if __cplusplus <= 199711L
#include <map>
#else
#include <unordered_map>
#endif
#include <gtest/gtest.h>

#include "../src/tqsllib.h"

#if __cplusplus <= 199711L
#define override
#define nullptr NULL
#define unordered_map map
#endif

class FileDecode : public ::testing::Test {
protected:
	std::string fixture;
	std::string mac_fixture;

	void SetUp() override {
		::testing::Test::SetUp();
		int retval;
		const char *path_env = getenv("DATADIR");
		ASSERT_NE(path_env, (void *)NULL);
		ASSERT_STRNE(path_env, "");
		fixture = path_env;
		mac_fixture = path_env;
		fixture += "/simple.cab";
		mac_fixture += "/simple-mac.cab";

		retval = tqsl_init();
		ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	}
};

TEST_F(FileDecode, OpenTest) {
	tQSL_Cabrillo cabrillo = nullptr;
	int retval;

	retval = tqsl_beginCabrillo(&cabrillo, fixture.c_str());
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	ASSERT_NE(cabrillo, (void *)NULL);

	retval = tqsl_endADIF(&cabrillo);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	ASSERT_EQ(cabrillo, (void *)NULL);
}

TEST_F(FileDecode, HeaderTest) {
	tQSL_Cabrillo cabrillo = nullptr;
	int retval;

	retval = tqsl_beginCabrillo(&cabrillo, fixture.c_str());
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();

	char contestName[20];
	retval = tqsl_getCabrilloContest(cabrillo, contestName, sizeof(contestName));
	contestName[sizeof(contestName)-1] = '\0';
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_STRCASEEQ(contestName, "cq-wpx-cw");

	TQSL_CABRILLO_FREQ_TYPE type;
	retval = tqsl_getCabrilloFreqType(cabrillo, &type);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_EQ(type, TQSL_CABRILLO_HF);

	int line;
	retval = tqsl_getCabrilloLine(cabrillo, &line);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_EQ(line, 2);

	retval = tqsl_endADIF(&cabrillo);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
}

TEST_F(FileDecode, RecordTest) {
	tQSL_Cabrillo cabrillo = nullptr;
	int retval;

	retval = tqsl_beginCabrillo(&cabrillo, fixture.c_str());
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();

	const char *records = tqsl_getCabrilloRecordText(cabrillo);
	//std::cout << records << std::endl;
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();

	std::unordered_map<std::string, std::string> fields;
	tqsl_cabrilloField field;
	TQSL_CABRILLO_ERROR_TYPE status = TQSL_CABRILLO_NO_ERROR;
	while(status == TQSL_CABRILLO_NO_ERROR) {
		retval = tqsl_getCabrilloField(cabrillo, &field, &status);
		ASSERT_EQ(retval, 0) << tqsl_getErrorString();
		fields[field.name] = field.value;
		//std::cout << field.name << " = " << field.value << std::endl;
	}
	EXPECT_EQ(status, TQSL_CABRILLO_EOR);
	EXPECT_EQ(fields.size(), 7U);
	EXPECT_STRCASEEQ(fields["MYCALL"].c_str(), "N0CAL");
	EXPECT_STRCASEEQ(fields["CALL"].c_str(), "W1AW");
	EXPECT_STRCASEEQ(fields["MODE"].c_str(), "ssb");
	EXPECT_STRCASEEQ(fields["QSO_DATE"].c_str(), "1999-03-06");
	EXPECT_STRCASEEQ(fields["TIME_ON"].c_str(), "0711");
	EXPECT_STRCASEEQ(fields["FREQ"].c_str(), "3.799000");
	EXPECT_STRCASEEQ(fields["BAND"].c_str(), "80m");

	int line;
	retval = tqsl_getCabrilloLine(cabrillo, &line);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_EQ(line, 3);

	retval = tqsl_endADIF(&cabrillo);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
}

TEST_F(FileDecode, MacHeaderTest) {
	tQSL_Cabrillo cabrillo = nullptr;
	int retval;

	retval = tqsl_beginCabrillo(&cabrillo, mac_fixture.c_str());
	//RecordProperty("Error", tqsl_getErrorString());
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();

	char contestName[20];
	retval = tqsl_getCabrilloContest(cabrillo, contestName, sizeof(contestName));
	contestName[sizeof(contestName)-1] = '\0';
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_STRCASEEQ(contestName, "cq-wpx-cw");

	TQSL_CABRILLO_FREQ_TYPE type;
	retval = tqsl_getCabrilloFreqType(cabrillo, &type);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_EQ(type, TQSL_CABRILLO_HF);

	int line;
	retval = tqsl_getCabrilloLine(cabrillo, &line);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
	EXPECT_EQ(line, 2);

	retval = tqsl_endADIF(&cabrillo);
	ASSERT_EQ(retval, 0) << tqsl_getErrorString();
}
