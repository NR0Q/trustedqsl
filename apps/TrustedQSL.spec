Summary: TrustedQSL ham-radio applications
Name: TrustedQSL
Version: 2.6.5
Release: 1%{?dist}
License: Custom BSD-like
Group: Applications/Ham Radio
Source: {{{ git_dir_pack }}}
BuildRoot: /var/tmp/%{name}-buildroot
Requires: wxwin
Buildrequires: epel-release desktop-file-utils make cmake gcc-c++ openssl-devel expat-devel zlib-devel zlib-devel curl-devel wxGTK3-devel

%description
The TrustedQSL applications are used for generating digitally signed
QSO records (records of Amateur Radio contacts). This package
contains the GUI application tqsl.

%prep
{{{ git_dir_setup_macro }}}

%build
cmake -DCMAKE_INSTALL_PREFIX=/usr ${TQSL_CONFIG_OPTS} .
make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
mkdir -p $RPM_BUILD_ROOT/usr/share/applications
cp tqsl.desktop $RPM_BUILD_ROOT/usr/share/applications/TrustedQSL-tqsl.desktop
mkdir -p $RPM_BUILD_ROOT/usr/share/pixmaps
cp icons/key48.png $RPM_BUILD_ROOT/usr/share/pixmaps/TrustedQSL.png

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc LICENSE

/usr/bin/tqsl
/usr/share/TrustedQSL/help/tqslapp
/usr/share/applications/TrustedQSL-tqsl.desktop
/usr/share/pixmaps/TrustedQSL.png
